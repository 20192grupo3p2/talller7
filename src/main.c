#include <stdio.h>
#include "../include/cabecera.h"

void imprimir(int arr[],int n){
  int i =0;
  printf("[");
  for (i = 0; i < n; i++) {
    printf("%d", *(arr+i));
    if(i<n-1){
	printf(","); }

  }
  printf("]\n");
}

int main(){
  int arr1[10] = {10,5,2,8,5,20,33,51,2,-1};
  int arr2[10] = {9,1,8,2,7,3,6,4,5,0};
  int arr3[10] = {5,2,7,65,23,99,10,100,45,18};

  comp c1 = comp1;
  comp c2 = comp2;
  comp c3 = comp_extrano;

  insertionSort(arr1,10,c1);
  insertionSort(arr2,10,c2);
  insertionSort(arr3,10,c3);

  imprimir(arr1,10);
  imprimir(arr2,10);
  imprimir(arr3,10);
}