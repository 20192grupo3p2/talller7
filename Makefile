bin/punterofuncion: obj/main.o obj/insertion.o 
	gcc $^ -o bin/punterofuncion -fsanitize=address,undefined
obj/main.o: src/main.c
	gcc -Wall -I include/ -c $^ -o obj/main.o -g
obj/insertion.o: src/insertion.c
	gcc -Wall -I include/ -c $^ -o obj/insertion.o -g

.PHONY: clean
clean:
	rm bin/* obj/*
